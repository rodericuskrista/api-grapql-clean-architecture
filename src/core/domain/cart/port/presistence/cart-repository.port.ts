import { CartDomain } from '@core/domain/cart/cart.domain';

export interface ICartRepositoryPort {
  storeCart(cartDomain: CartDomain): Promise<CartDomain>;
}
