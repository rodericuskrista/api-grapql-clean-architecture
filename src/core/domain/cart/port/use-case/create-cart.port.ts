/* eslint-disable @typescript-eslint/no-unused-vars */

import { IsNotEmpty, IsNumber, IsString, Min } from 'class-validator';

export class CreateCartPort {
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  quantity: number;

  @IsNotEmpty()
  @IsString()
  productId: string;
}
