import { ProductEntity } from '@core/presistence/product/entity/product.entity';
/* eslint-disable @typescript-eslint/no-unused-vars */
export interface CreateCartPayload {
  quantity: number;
  amount: number;
  product: ProductEntity;
}
