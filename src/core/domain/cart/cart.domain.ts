import { IsInstance, IsString, IsNumber, IsOptional } from 'class-validator';
import { ProductEntity } from '@core/presistence/product/entity/product.entity';
import { CreateCartPayload } from '@core/domain/cart/type/create-cart.payload';

export class CartDomain {
  @IsOptional()
  @IsString()
  protected id: string;

  @IsOptional()
  @IsNumber()
  protected quantity: number;

  @IsOptional()
  @IsNumber()
  protected amount: number;

  @IsOptional()
  @IsInstance(ProductEntity)
  protected product: ProductEntity;

  public constructor(opts: Partial<CartDomain>) {
    Object.assign(this, opts);
  }

  public static create(payload: CreateCartPayload) {
    return new CartDomain(payload);
  }
}
