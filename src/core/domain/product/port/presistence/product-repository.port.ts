import { ProductDomain } from '@core/domain/product/product.domain';

export interface IProductRepositoryPort {
  storeProduct(productDomain: ProductDomain): Promise<ProductDomain>;
  findProductById(productId: string): Promise<ProductDomain>;
}
