import { CreateProductPayload } from '@core/domain/product/type/create-product.payload';
import { IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class ProductDomain {
  @IsOptional()
  @IsString()
  protected id: string;

  @IsOptional()
  @IsString()
  protected name: string;

  @IsOptional()
  @IsNumber()
  @Min(0)
  protected price: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  protected stock: number;

  @IsOptional()
  @IsString()
  protected description: string;

  public constructor(opts: Partial<ProductDomain>) {
    Object.assign(this, opts);
  }

  public static create(payload: CreateProductPayload) {
    return new ProductDomain(payload);
  }
}
