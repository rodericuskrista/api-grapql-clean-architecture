export interface CreateProductPayload {
  name: string;
  price: number;
  stock: number;
  description?: string;
}
