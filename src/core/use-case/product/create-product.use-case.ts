import { ProductDomain } from '@core/domain/product/product.domain';
import { Inject } from '@nestjs/common';
import { IProductRepositoryPort } from '@core/domain/product/port/presistence/product-repository.port';
import { UseCase } from '@libs/contract/use-case';
import { CreateProductPort } from '@core/domain/product/port/use-case/create-product.port';

export class CreateProductUseCase
  implements UseCase<CreateProductPort, ProductDomain>
{
  constructor(
    @Inject('PRODUCT_REPOSITORY')
    private readonly productRepository: IProductRepositoryPort,
  ) {}

  async execute(product?: CreateProductPort): Promise<ProductDomain> {
    const productDomain = ProductDomain.create(product);
    return await this.productRepository.storeProduct(productDomain);
  }
}
