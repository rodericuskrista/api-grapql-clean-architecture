import { ProductDomain } from '@core/domain/product/product.domain';
import { UseCase } from '@libs/contract/use-case';
import { Inject, NotFoundException } from '@nestjs/common';
import { IProductRepositoryPort } from '@core/domain/product/port/presistence/product-repository.port';

export class FindProductByIdUseCase implements UseCase<string, ProductDomain> {
  constructor(
    @Inject('PRODUCT_REPOSITORY')
    private readonly productRepository: IProductRepositoryPort,
  ) {}

  async execute(productId?: string): Promise<ProductDomain> {
    const foundProductDomain = await this.productRepository.findProductById(
      productId,
    );
    if (!foundProductDomain) throw new NotFoundException();
    return foundProductDomain;
  }
}
