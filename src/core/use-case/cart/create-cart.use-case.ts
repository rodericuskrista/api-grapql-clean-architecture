import { ProductMapper } from '@core/presistence/product/mapper/product.mapper';
import { CartDomain } from '@core/domain/cart/cart.domain';
import { CreateCartPort } from '@core/domain/cart/port/use-case/create-cart.port';
import { Inject, NotFoundException } from '@nestjs/common';
import { ICartRepositoryPort } from '@core/domain/cart/port/presistence/cart-repository.port';
import { IProductRepositoryPort } from '@core/domain/product/port/presistence/product-repository.port';
import { UseCase } from '@libs/contract/use-case';

export class CreateCartUseCase implements UseCase<CreateCartPort, CartDomain> {
  constructor(
    @Inject('CART_REPOSITORY')
    private readonly cartRepository: ICartRepositoryPort,
    @Inject('PRODUCT_REPOSITORY')
    private readonly productRepository: IProductRepositoryPort,
  ) {}

  async execute(cart?: CreateCartPort): Promise<CartDomain> {
    const foundProductDomain = await this.productRepository.findProductById(
      cart.productId,
    );
    if (!foundProductDomain) throw new NotFoundException();
    const foundProductEntity = ProductMapper.DomainToEntity(foundProductDomain);
    const cartDomain = CartDomain.create({
      quantity: cart.quantity,
      amount: foundProductEntity.price * cart.quantity,
      product: foundProductEntity,
    });
    return await this.cartRepository.storeCart(cartDomain);
  }
}
