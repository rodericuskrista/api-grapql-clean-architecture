import { CartMapper } from '@core/presistence/cart/mapper/cart.mapper';
import { EntityRepository, Repository } from 'typeorm';
import { CartEntity } from '@core/presistence/cart/entity/cart.entity';
import { ICartRepositoryPort } from '@core/domain/cart/port/presistence/cart-repository.port';
import { CartDomain } from '@core/domain/cart/cart.domain';

@EntityRepository(CartEntity)
export class CartRepository
  extends Repository<CartEntity>
  implements ICartRepositoryPort
{
  async storeCart(cartDomain: CartDomain): Promise<CartDomain> {
    const cartEntity = CartMapper.DomainToEntity(cartDomain);
    const savedCartEntity = await this.save(cartEntity);
    return CartMapper.EntityToDomain(savedCartEntity);
  }
}
