import { CartEntity } from '@core/presistence/cart/entity/cart.entity';
import { CartDomain } from '@core/domain/cart/cart.domain';

export class CartMapper {
  static DomainToEntity(cartDomain: Partial<CartDomain>): CartEntity {
    return new CartEntity(cartDomain);
  }

  static EntityToDomain(cartEntity: Partial<CartEntity>): CartDomain {
    return new CartDomain(cartEntity);
  }
}
