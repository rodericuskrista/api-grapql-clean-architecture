import { ProductMapper } from '@core/presistence/product/mapper/product.mapper';
import { EntityRepository, Repository } from 'typeorm';
import { ProductEntity } from '@core/presistence/product/entity/product.entity';
import { IProductRepositoryPort } from '@core/domain/product/port/presistence/product-repository.port';
import { ProductDomain } from '@core/domain/product/product.domain';

@EntityRepository(ProductEntity)
export class ProductRepository
  extends Repository<ProductEntity>
  implements IProductRepositoryPort
{
  async findProductById(productId: string): Promise<ProductDomain> {
    const foundProductEntity = await this.findOne({ where: { id: productId } });
    return ProductMapper.EntityToDomain(foundProductEntity);
  }

  async storeProduct(productDomain: ProductDomain): Promise<ProductDomain> {
    const productEntity = ProductMapper.DomainToEntity(productDomain);
    const savedProductEntity = await this.save(productEntity);
    return ProductMapper.EntityToDomain(savedProductEntity);
  }
}
