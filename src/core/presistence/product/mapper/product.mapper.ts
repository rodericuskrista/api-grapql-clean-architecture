import { ProductEntity } from '@core/presistence/product/entity/product.entity';
import { ProductDomain } from '@core/domain/product/product.domain';

export class ProductMapper {
  static DomainToEntity(productDomain: Partial<ProductDomain>): ProductEntity {
    return new ProductEntity(productDomain);
  }

  static EntityToDomain(productEntity: Partial<ProductEntity>): ProductDomain {
    return new ProductDomain(productEntity);
  }
}
