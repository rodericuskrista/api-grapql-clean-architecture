import { Body, Controller, Inject, Post } from '@nestjs/common';
import { CreateCartUseCase } from '@core/use-case/cart/create-cart.use-case';
import { CreateCartPort } from '@core/domain/cart/port/use-case/create-cart.port';

@Controller('carts')
export class CartController {
  constructor(
    @Inject('CREATE_CART_USE_CASE')
    private readonly createCartUseCase: CreateCartUseCase,
  ) {}

  @Post('create')
  createCart(@Body() cart: CreateCartPort) {
    return this.createCartUseCase.execute(cart);
  }
}
