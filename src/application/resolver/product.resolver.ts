/* eslint-disable @typescript-eslint/no-unused-vars */
import { ProductEntity } from '@core/presistence/product/entity/product.entity';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { CreateProductUseCase } from '@core/use-case/product/create-product.use-case';
import { CreateProductPort } from '@core/domain/product/port/use-case/create-product.port';

@Resolver()
export class ProductResolver {
  constructor(
    @Inject('CREATE_PRODUCT_USE_CASE')
    private readonly createProductUseCase: CreateProductUseCase,
  ) {}

  @Query()
  sayHello(): string {
    return 'Hello World!';
  }

  @Mutation()
  createProduct(
    @Args('createProductInput') createProductPort: CreateProductPort,
  ) {
    return this.createProductUseCase.execute(createProductPort);
  }
}
