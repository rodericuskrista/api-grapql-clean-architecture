/* eslint-disable @typescript-eslint/no-unused-vars */
import { Args, Mutation, Resolver, Query } from '@nestjs/graphql';
import { Inject } from '@nestjs/common';
import { CreateCartUseCase } from '@core/use-case/cart/create-cart.use-case';
import { CreateCartPort } from '@core/domain/cart/port/use-case/create-cart.port';

@Resolver()
export class CartResolver {
  constructor(
    @Inject('CREATE_CART_USE_CASE')
    private readonly createCartUseCase: CreateCartUseCase,
  ) {}

  @Query()
  sayHello(): string {
    return 'Hello World!';
  }

  @Mutation()
  createCart(@Args('createCartInput') createCartPort: CreateCartPort) {
    return this.createCartUseCase.execute(createCartPort);
  }
}
